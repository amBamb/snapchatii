package com.amber.snapchat;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by Student on 5/28/2016.
 */
public class FriendListFragment extends Fragment{

    private ArrayList<String> friends;
    private ArrayAdapter<String> friendListAdapter;

    public FriendListFragment(){

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_friend_list, container, false);

        final Uri imageToSend = getActivity().getIntent().getParcelableExtra("ImageURI");

        friends = new ArrayList<String>();
        friendListAdapter = new ArrayAdapter<String>(
                getActivity(), android.R.layout.simple_list_item_1, friends);

        ListView friendList = (ListView) view.findViewById(R.id.friendList);
        friendList.setAdapter(friendListAdapter);

        String currentUser = Backendless.UserService.loggedInUser();
        Backendless.Persistence.of(BackendlessUser.class).findById(currentUser, new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser user) {
                Objects[] friendObjects = (Object[]) user.getProperty("friends");
                if (friendObjects.length > 0 ) {
                    BackendlessUser [] friendArray = (BackendlessUser[]) friendObjects;
                    for (BackendlessUser friend : friendArray) {
                        String name = friend.getProperty("name").toString();
                        friends.add(name);
                        friendListAdapter.notifyDataSetChanged();
                    }
                }
                final String currentUserName = (String) user.getProperty("name");
                friendList.setOnItemClickListener(new ArrayAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdpaterView<?> parent, View view, int position, long id){
                        String friendName = (String) parent.getItemAtPosition(position);
                        sendImageToFriend(currentUserName, friendName, imageToSend);
                    }
                });
            }

            @Override
            public void handleFault(BackendlessFault fault) {

            }
        });

        return view;
    }

    private void sendImageToFriend(String currentUser, String toUser , Uri imageUri){
        Intent intent = new Intent(getActivity(), DemoService.class);
        intent.setAction(Consatnts.ACTION_SEND_PHOTO);
        intent.putExtra("fromUser", currentUser);
        intent.putExtra("toUser", toUser);
        intent.putExtra("imageUri", iimageURI);
        getActivity().startService(intent);

    }
}
