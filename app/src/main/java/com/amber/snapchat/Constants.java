package com.amber.snapchat;

public class Constants {

    public static final String ACTION_ADD_FRIEND = "com.amber.snapchat.ADD_FRIEND";
    public static final String ACTION_SEND_FRIEND_REQUEST = "com.amber.snapchat.SEND_FRIEND_REQUEST";
    public static final String ACTION_SEND_PHOTO = "com.amber.snapchat.SEND_PHOTO";

    public static final String BROADCAST_ADD_FRIEND_SUCCESS = "com.amber.snapchat.ADD_FRIEND_SUCCESS";
    public static final String BROADCAST_ADD_FRIEND_FAILURE = "com.amber.snapchat.ADD_FRIEND_FAILURE";

    public static final String BROADCAST_FRIEND_REQUEST_SUCCESS = "com.amber.snapchat.FRIEND_REQUEST_SUCCESS";
    public static final String BROADCAST_FRIEND_REQUEST_FAILURE = "com.amber.snapchat.FRIEND_REQUEST_FAILURE";
}
