package com.amber.snapchat;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.DialogPreference;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;

/**
 * Created by Student on 5/30/2016.
 */
public class AddFriendFragment {

    public  AddFriendFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_add_friend, container, false);

        Button addFriendButton = (Button) view.findViewById(R.id.addFriendButton);
        addFriendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setMessage("Add a Friend.");

                final EditText inputField = new EditText(getActivity());
                alertDialog.setView(inputField);

                alertDialog.setNegativeButton("Cancel", new DialogPreference.OnClickListener() {
                    @Override
                    public void OnClick(DialogInterface dialog, int which) {
                        //something
                    }
                });

                alertDialog.setPositiveButton("Add Friend", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        sendFriendRequest(inputField.getText().toString());
                        Toast.makeText(getActivity(), "Add Friend!", Toast.LENGTH_SHORT).show();
                    }
                });

                alertDialog.create();
                alertDialog.show();
            }
        });

        return view;

    }

    private void sendFriendRequest(final String friendName){
        String currentId = Backendless.UserService.loggedInUser();
        Backendless.Persistence.of(BackendlessUser.class).findById(currentId, new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser currUser) {
                Intent intent = new Intent(getActivity(), DemoService.class);
                intent.setAction(Constants.ACTION_SEND_FRIEND_REQUEST);
                intent.putExtra("fromUser", currUser.getProperty("name").toString());
                intent.putExtra("toUser", friendName);

                getActivity().startService(intent);

            }

            @Override
            public void handleFault(BackendlessFault fault) {

            }
        });




    }
}
