package com.amber.snapchat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

/**
 * Created by Student on 5/31/2016.
 */
public class FriendRequestActivity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FriendRequestFragment friendRequest = new FriendRequestFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container, friendRequest).commit();


    }
}
