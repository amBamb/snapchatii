package com.amber.snapchat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.backendless.Backendless;

public class MainActivity extends AppCompatActivity {

    public static final String APP_ID = "09829620-3642-663A-FFC4-3CC884A31B00";
    public static final String SECRET_KEY = "37DAB379-69F5-71E1-FFB8-7F19EF14D000";
    public static final String VERSION= "v1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Backendless.initApp(this, APP_ID, SECRET_KEY, VERSION);
        if (Backendless.UserService.loggedInUser() == "") {
            MainMenuFragment mainMenu = new MainMenuFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container, mainMenu).commit();
        } else {
            LoggedInFragment loggedIn = new LoggedInFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container, loggedIn).commit();
        }
    }

    }

